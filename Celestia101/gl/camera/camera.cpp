#include "camera.h"
#include <GL/freeglut.h>

//----------------------------------------------------------
///Class: Camera
Camera::Camera()
{
	//dT = 0.0f;
	LookAt = glm::vec4(1.0f, 0.0f, 0.0f, 0.0f);
	//Velocity = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	//EyePosition = glm::vec4(0.0f, 0.0f, -50.0f, 0.0f);
	Up = glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
}
Camera::~Camera()
{

}

glm::vec3 Camera::v4tov3(glm::vec4 v4)
{
	glm::vec3 v3(v4);
	return v3;
}



void Camera::AcceptValues(float Pitch, float Yaw, glm::vec4 Position, glm::vec4 Look)
{
	degPitch = Pitch;
	degYaw = Yaw;
	EyePosition = Position;
	LookAt = Look;
}


void Camera::PlaceCamera(int m)
{

	// calcYawAndPitch();

	// LookAt =
	// 	glm::vec4(
	// 		0.0f, //X,
	// 		0.0f, //Y,
	// 		1.0f, //Z
	// 		0.0f  //G
	// );
	switch(m)
	{
		case Pl :
			view =
			glm::lookAt(
				v4tov3(EyePosition),   //eye
				v4tov3(EyePosition) + v4tov3(LookAt),//center
				v4tov3(Up)    //up
			);
		break;
		case Follow :
			view =
			glm::lookAt(
				v4tov3(EyePosition),   //eye
				v4tov3(LookAt),//center
				v4tov3(Up)    //up
			);
		break;
	}
}

// void Camera::chgVel(float dVx, float dVy, float dVz)
// {
// 	// calcYawAndPitch();



// 	glm::vec4 additionalVelocity(+dVx, +dVy, +dVz, 0.0f);
// 	//additionalVelocity *= LookAt;
// 	Velocity += GiveRotMatrix(mvment)*additionalVelocity;

// 	// Velocity.x -= dVx;
// 	// Velocity.y += dVy;
// 	// Velocity.z -= dVz;
// }


glm::mat4 Camera::GiveRotMatrix(int mode)
{
	glm::vec3 axis_z(0.0f, 0.0f, 1.0f), axis_y(0.0f, 1.0f, 0.0f), axis_x(1.0f, 0.0f, 0.0f);
	glm::mat4 m_transform =
		glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 0.0))
		* glm::rotate( glm::mat4(1.0f), (float)(radPitch), axis_x)
		* glm::rotate( glm::mat4(1.0f), (float)(radYaw  ), axis_y);

		return m_transform;
}

glm::mat4 Camera::getView(void) const
{
	return view;
}


// void Camera::addPitch(float additionalPitch)
// {
// 	// int Sign = Pitch>0?1:(Pitch==0?0:-1);
// 	// Pitch += (int)NewPitch;
// 	degPitch += additionalPitch;
// }
// float Camera::getPitch(void) const
// {
// 	return degPitch;
// }

// void Camera::addYaw(float additionalYaw)
// {
// 	// Yaw += (int)NewYaw;
// 	degYaw += additionalYaw;
// }
// float Camera::getYaw(void) const
// {
// 	return degYaw;
// }

glm::vec3 Camera::getLookAt(void)
{
	return v4tov3(LookAt);
}
//----------------------------------------------------------
