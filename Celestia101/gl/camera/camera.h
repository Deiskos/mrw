#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include <stdio.h>

#include <math.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Camera
{
private:
	float degYaw = 0.0f, degPitch = 0.0f,
		  	radYaw = 0.0f, radPitch = 0.0f;
	glm::vec4 LookAt = glm::vec4(1.0f, 0.0f, 0.0f, 0.0f);
	glm::vec4 Velocity = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f), EyePosition = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	glm::vec4 Up = glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
	glm::mat4 view;

	//Auxillary
	glm::vec3 v4tov3(glm::vec4);

	//Realistic movement
	float dT;
	// void calcYawAndPitch();
	enum UsingModes
	{
		Pl = 0,
		Follow = 1
	};
public:
	//ctor&dtor
	Camera();
	~Camera();

	//Matrices
	glm::mat4 GiveRotMatrix(int);
	glm::mat4 getView(void) const;

	//OpenGL stuff
	void PlaceCamera(int);

	//Realistic movement
	void calculatePosition();
	void chgVel(float dVx, float dVy, float dVz); //Simply adds components to Velocity


	// void addPitch(float);
	// float getPitch(void) const;
	// void addYaw(float);
	// float getYaw(void) const;

	void AcceptValues(float, float, glm::vec4, glm::vec4);

	// void setEyePosition(glm::vec3);
	// glm::vec3 getEyePosition(void) const;
	// void setLookVector(glm::vec3);
	glm::vec3 getLookAt(void);
};

#endif // CAMERA_H
