#ifndef OBJECT_H
#define OBJECT_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include "../../utils/datastructures.h"


struct TElements
{
	GLushort vIndex;
	GLushort tIndex;
};

///Class: TObject
struct TObject
{
	private:
		GLuint tex_id;
		int mode;

		GLuint vbo_attrib, ibo_elements;
		glm::vec3 Position;
		glm::mat4 model, mvp; //, projection;

	public:
		int MaterialID;
		std::vector<TAttributes> vAttributes; int sizeVBO;
		std::vector<GLushort> vElements;			int sizeIBO;
		char name[512];
		void Visualize();
		TObject();
		int Initialize(const char*, const char*);
		//void objRotate3f(float degX, float degY, float degZ);
		//--------------------------------------------------------------------
		///Get/Set methods
		void setPosition(glm::vec3);
		glm::vec3 getPosition(void) const;
};

#endif