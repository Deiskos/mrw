#ifndef MATERIAL_H
#define MATERIAL_H

class TMaterial
{
	private:
		char Name[256];
		/*mtl example
			# Blender MTL File: 'None'
			# Material Count: 1

			newmtl None
			Ns 0

			#Ka r g b 
			#Ambient reflectivity of material.
			Ka 0.000000 0.000000 0.000000

			#Kd r g b 
			#Diffuse reflectivity of material. 
			Kd 0.8 0.8 0.8

			#Ks r g b
			#Specular reflectivity of material
			Ks 0.8 0.8 0.8

			#d factor
			#dissolve factor, maybe can be used as transparency
			d 1

			#illum illum_#
			#Illumination model, range form 0 to 10
			illum 2
		*/

	public:
		TMaterial();
		~TMaterial();

};

#endif