#include "player.h"

Player::Player(void)
{
	cameramode = 0;

	mass = 60.0f;
	slowfactor = 0.1f;

	degPitch = 0.0f;
	degYaw = -90.0f;

	vVelocity = glm::vec3(0.0f, 0.0f, 0.0f);
	vAcceleration = glm::vec3(0.0f, 0.0f, 0.0f);
	vPosition = glm::vec3(0.0f, 0.0f, 10.0f);
}

void Player::HookCamPtr(Camera *Cam)
{
	CamPtr = Cam;
}

void Player::addYaw(float f)
{
	degYaw += f;
}
void Player::addPitch(float f)
{
	degPitch += f;
}

// glm::mat3 Player::GiveRotMatrix(int mode)
// {
// 	glm::vec3 axis_z(0.0f, 0.0f, 1.0f), axis_y(0.0f, 1.0f, 0.0f), axis_x(1.0f, 0.0f, 0.0f);
// 	glm::mat4 m_transform =
// 		glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 0.0))
// 		* glm::rotate(glm::mat4(1.0f), (float)(radPitch), axis_x)
// 		* glm::rotate(glm::mat4(1.0f), (float)(!mode ? radYaw : -radYaw), axis_y);

// 		return m_transform;
// }

void Player::chgVel(float dVx, float dVy, float dVz)
{
	YawPitch_to_rad();

	glm::vec3 axis_z(0.0f, 0.0f, 1.0f), axis_y(0.0f, 1.0f, 0.0f), axis_x(1.0f, 0.0f, 0.0f);
	glm::mat4 mat =
		glm::translate(glm::mat4(1.0f), nvPlayerLookAt)
		* glm::rotate( glm::mat4(1.0f), (float)(-radPitch), axis_z)
		* glm::rotate( glm::mat4(1.0f), (float)(radYaw  ), axis_y);

	glm::vec4 additionalVelocity(+dVx, +dVy, -dVz, 0.0f);
	additionalVelocity = additionalVelocity * mat;
	glm::vec3 addv3Vel (additionalVelocity);
	vVelocity += addv3Vel;
}

void Player::DeltaPitchYaw(float dP, float dY)
{
	degPitch += dP;
	degYaw += dY;
}

void Player::KBD(char c)
{
	switch (c)
	{
		case 'a':
			chgVel(0.0, 0.0, +0.5);
		break;
		case 'd':
			chgVel(0.0, 0.0, -0.5);
		break;
		case 'w':
			chgVel(+0.5, 0.0, 0.0);
		break;
		case 's':
			chgVel(-0.5, 0.0, 0.0);
		break;
		case 'r':
			chgVel(0.0, 0.5, 0.0);
		break;
		case 'f':
			chgVel(0.0, -0.5, 0.0);
		break;

		case 'i':
			addPitch(-10.0);
		break;
		case 'j':
			addYaw(-10.0);
		break;
		case 'k':
			addPitch(+10.0);
		break;
		case 'l':
			addYaw(+10.0);
		break;

		case 't' :
			cameramode = !cameramode;
		break;

	}
}

void Player::YawPitch_to_rad()
{
	radYaw   = (degYaw   * M_PI) / 180.0;
	radPitch = (degPitch * M_PI) / 180.0;
}

int Player::Process()
{
	YawPitch_to_rad();

	nvPlayerLookAt.x = cosf(radPitch) * cosf(radYaw);
	nvPlayerLookAt.y = sinf(radPitch);
	nvPlayerLookAt.z = cosf(radPitch) * sinf(radYaw);

	// printf("nvPlayerLookAt:\n  %f\n  %f\n  %f\n\n", nvPlayerLookAt.x, nvPlayerLookAt.y, nvPlayerLookAt.z);
	// printf("degPitch: %f\n", degPitch);
	// printf("degYaw: %f\n", degYaw);
	// system("clear");



	// x=r\sin\theta\cos\varphi, \\
	// y=r\sin\theta\sin\varphi, \\
	// z=r\cos\theta.


	vVelocity -= vVelocity * slowfactor;
	vPosition += vVelocity * 0.16f;


	// nvCameraLookAt = nvPlayerLookAt;
	// vCameraPosition = vPosition
	glm::vec4 v4Position(vPosition, 0.0f);
	glm::vec4 nv4PlayerLookAt(nvPlayerLookAt, 0.0f);
	if(cameramode)
	{
		nv4PlayerLookAt = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	}
	CamPtr->AcceptValues(degPitch, degYaw, v4Position, nv4PlayerLookAt);
	return cameramode;
}

glm::vec3 Player::getPosition(void) const
{
	return vPosition;
}
