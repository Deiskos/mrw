#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED
#include "../../gl/camera/camera.h"

#include <math.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Player
{
private:
	Camera *CamPtr;
	bool cameramode;

	float mass, slowfactor;

	float degPitch, degYaw;
	float radPitch, radYaw;
	glm::vec3 vPosition, vVelocity, vAcceleration;
	glm::vec3 nvPlayerLookAt, nvCameraLookAt;

	//POSITIONING
	void chgVel(float, float, float);
	void addPitch(float);
	void addYaw(float);


	//AUX
	void YawPitch_to_rad();




public:
	Player();
	// ~Player();	
	void DeltaPitchYaw(float, float);
	void HookCamPtr(Camera*);
	void KBD(char);
	int Process();

	glm::vec3 getPosition(void) const;
};

#endif //PLAYER_H