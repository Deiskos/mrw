#include "utils.h"
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <vector>
#include <GL/glew.h>
//TODO (02.01.2016#deiskos#0) Add ..mtl support, make everything readable
void TUtility::OpenFile(const char* fname)
{
	file = fopen(fname, "r");
	if(NULL == file)
	{
		fprintf(stderr, "File \"%s\" not found\n", fname);
		return;
	}

	SetupFILE(fname);
}

void TUtility::Reset(void)
{
	sizeIBO = sizeVBO = 0;
	file = NULL;
}

void TUtility::SetupFILE(const char* fname)
{
	const char * l = strrchr(fname, '.'); //_Last_ occurence of element in cstring
	unsigned int lastdot =  l - fname;
	char extension[] = "aaaa";
	memcpy(&extension, &fname[lastdot+1], strlen(fname) - lastdot);

	if(strcmp(extension, "obj") == 0)
	{
		FileExt = OBJ;
		printf("%s file provided\n", extension);
		return;
	} 
	else if(strcmp(extension, "mtl") == 0)
	{
		printf("%s file provided\n", extension);
		FileExt = MTL;
		return;
	}
	else
	{
		exit(WRONG_FILE);
	}
}

void TUtility::SetupObject(TObject* Object)
{
	char dummy[512];
	if(file != NULL)
	{
		std::vector<Tval3f> vVert;
		std::vector<Tval2f> vTexCoord;
		std::vector<Tval3f> vNormals;
		std::vector<TAttributes> vAttributes;
		

		char c;
		while((c = fgetc(file) != EOF)&&!feof(file))
		{
			if(c == '#')
			{
				fgets(dummy, 512, file);
			}
			fseek(file, -1, SEEK_CUR);

			fscanf(file, "%s", &dummy[0]);
			if(strcmp(dummy, "v") == 0)
			{
				Tval3f Vertex;
				fscanf(
					file,
					"%f %f %f",
					&Vertex.val[0],
					&Vertex.val[1],
					&Vertex.val[2]
				);
				vVert.push_back(Vertex);
			} 
			else if (strcmp(dummy, "vt") == 0)
			{
				Tval2f TexCoord;
				fscanf(
					file,
					"%f %f",
					&TexCoord.val[0],
					&TexCoord.val[1]
				);
				vTexCoord.push_back(TexCoord);
			}
			else if(strcmp(dummy, "vn") == 0)
			{
				Tval3f Normal;
				fscanf(
					file,
					"%f %f %f",
					&Normal.val[0],
					&Normal.val[1],
					&Normal.val[2]
				);
				vNormals.push_back(Normal);
			}
			strcpy(dummy, "");
		}


		std::vector<TFace> vFaces; 
		rewind(file); 
		int id = 0; 
		TFace Face; 
		while((c = fgetc(file) != EOF)&&!feof(file))
		{
			fseek(file, -1, SEEK_CUR);
			fscanf(file, "%s", &dummy[0]);

			if(strcmp(dummy, "f") == 0)
			{
				for(int j = 0; j < 3; ++j)
				{
					//elements assembling
					int iv, ivt, ivn=1; //!!!
					fscanf(file, "%i/%i", &iv, &ivt);
					Face.Renew(iv - 1, ivt - 1, ivn - 1);
					Face.setID(id);
					vFaces.push_back(Face);
					bool match = false;
					for(int i = 0; i < vFaces.size() - 1; ++i)
					{
						if(Face == vFaces[i])
						{
							vFaces[vFaces.size()-1] = vFaces[i];
							match = true;
							/*I want to */break; //free
						}
					}
					if(false == match)
					{
						id++;
					}
				}
			}
			strcpy(dummy, "");
		}

		std::vector<TFace> vFacesOpt;
		std::vector<GLushort> vIBO;
		int m = -1;
		for(int i = 0; i < vFaces.size(); ++i)
		{
			vIBO.push_back(vFaces[i].getID());

			if(vFaces[i].getID() > m)
			{
				vFacesOpt.push_back(vFaces[i]);
				m++;
			}
		}

		std::vector<TAttributes> vVBO;
		TAttributes Attr;
		for(int i = 0; i < vFacesOpt.size(); ++i)
		{
			Attr.coord3d[0] = vVert[vFacesOpt[i].iv].val[0];
			Attr.coord3d[1] = vVert[vFacesOpt[i].iv].val[1];
			Attr.coord3d[2] = vVert[vFacesOpt[i].iv].val[2];

			Attr.texcoord[0] = vTexCoord[vFacesOpt[i].ivt].val[0];
			Attr.texcoord[1] = vTexCoord[vFacesOpt[i].ivt].val[1];

			vVBO.push_back(Attr);
		}

		Object->vElements = vIBO;
		Object->vAttributes = vVBO;

		for(int i = 0; i < vVBO.size(); ++i)
		{
			printf("VBO\n{\n  id:%i\n  x:%f,y:%f,z:%f\n}\n\n",
				i,
				vVBO[i].coord3d[0],
				vVBO[i].coord3d[1],
				vVBO[i].coord3d[2]
			);
		}

	}	
}


//--------------------------------------------------------------------


//--------------------------------------------------------------------
//shader_utils.cpp

/**
 * Store all the file's contents in memory, useful to pass shaders
 * source code to OpenGL
 */
/* Problem:
 *  We should close the input file before the return NULL; statements but this would lead to a lot of repetition (DRY)
 *   -you could solve this by using goto or by abusing switch/for/while + break or by building an if else mess
 *  better solution: let the user handle the File: char* file_read(const FILE* input)
*/
char* file_read(const char* filename)
{
  FILE* input = fopen(filename, "rb");
  if(input == NULL) return NULL;

  if(fseek(input, 0, SEEK_END) == -1) return NULL;
  long size = ftell(input);
  if(size == -1) return NULL;
  if(fseek(input, 0, SEEK_SET) == -1) return NULL;

  /*if using c-compiler: dont cast malloc's return value*/
  char *content = (char*) malloc( (size_t) size +1  );
  if(content == NULL) return NULL;

  fread(content, 1, (size_t)size, input);
  if(ferror(input)) {
    free(content);
    return NULL;
  }

  fclose(input);
  content[size] = '\0';
  return content;
}

/**
 * Display compilation errors from the OpenGL shader compiler
 */
void print_log(GLuint object)
{
  GLint log_length = 0;
  if(glIsShader(object))
    glGetShaderiv(object, GL_INFO_LOG_LENGTH, &log_length);
  else if(glIsProgram(object))
    glGetProgramiv(object, GL_INFO_LOG_LENGTH, &log_length);
  else
  {
    fprintf(stderr,"printlog: Not a shader or a program\n");
    return;
  }

  char* log = (char*)malloc(log_length);

  if(glIsShader(object))
    glGetShaderInfoLog(object, log_length, NULL, log);
  else if(glIsProgram(object))
    glGetProgramInfoLog(object, log_length, NULL, log);

  fprintf(stderr, "%s", log);
  free(log);
}

/**
 * Compile the shader from file 'filename', with error handling
 */
GLuint create_shader(const char* filename, GLenum type)
{
  const GLchar* source = file_read(filename);
  if (source == NULL) {
    fprintf(stderr, "Error opening %s: ", filename); perror("");
    return 0;
  }
  GLuint res = glCreateShader(type);
  const GLchar* sources[2] = {
#ifdef GL_ES_VERSION_2_0
    "#version 100\n"
    "#define GLES2\n",
#else
    "#version 130\n",
#endif
    source };
  glShaderSource(res, 2, sources, NULL);
  free((void*)source);

  glCompileShader(res);
  GLint compile_ok = GL_FALSE;
  glGetShaderiv(res, GL_COMPILE_STATUS, &compile_ok);
  if (compile_ok == GL_FALSE) {
    fprintf(stderr, "%s:", filename);
    print_log(res);
    glDeleteShader(res);
    return 0;
  }

  return res;
}