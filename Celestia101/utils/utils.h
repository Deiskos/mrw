#ifndef UTILS_H
#define UTILS_H

#include "../gl/tobject/tobject.h"
#include <vector>
#include <cstdio>
#include <GL/glew.h>
#include <glm/glm.hpp>

//--------------------------------------------------------------------
///Technical everything
///MOVED TO DATASTRUCTURES.O

//--------------------------------------------------------------------
///Functions
//TODO (05.01.2016#deiskos#0) Понять что я тут написал, переделать чтобы было логично, загрузить сферку.


class TUtility
{
private:
	void SetupFILE(const char*);
	int FileExt;
	int sizeIBO, sizeVBO;

public:
	FILE* file;

	void OpenFile(const char*);
	void Reset(void);

	void SetupObject(TObject*);



	TUtility()
	{
		sizeIBO = sizeVBO = 0;
		file = NULL;
		FileExt = -1;
	}
};

unsigned int linecount(const char *name);
void scanvbo(const char *name, attributes_3f3f arr[]);
void scanibo(const char *name, GLushort el[]);

char* file_read(const char*);
void print_log(GLuint);
GLuint create_shader(const char*, GLenum);


//--------------------------------------------------------------------

#endif // UTILS
