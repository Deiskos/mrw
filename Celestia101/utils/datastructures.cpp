#include "datastructures.h"

void TFace::setID(int identificator)
{
	id = identificator;
}
int TFace::getID() const
{
	return id;
}
void TFace::Renew(int _iv, int _ivt, int _ivn)
{
	iv = _iv;
	ivt = _ivt;
	ivn = _ivn;
}
TFace::TFace(void)
{

}
void TFace::operator=(TFace rvalue)
{
	this->id  = rvalue.id;
	this->iv  = rvalue.iv;
	this->ivn = rvalue.ivn;
	this->ivt = rvalue.ivt;
}
bool TFace::operator==(TFace rvalue)
{
	return (
		( this->iv == rvalue.iv )&&
		(this->ivt == rvalue.ivt)&&
		(this->ivn == rvalue.ivn)
	);
}