#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H

#include <vector>
#include <cstdio>
#include <GL/glew.h>
#include <glm/glm.hpp>

//--------------------------------------------------------------------
///Technical everything


struct TScreenCoords
{
	int u, v;
};


struct Tval2f
{
	GLfloat val[2];
};

struct Tval3f
{
	GLfloat val[3];
};

class TFace
{
private:
	int id;
public:
	int iv, ivt, ivn;
	void setID(int identificator);
	int getID() const;
	void Renew(int _iv, int _ivt, int _ivn);
	TFace(void);
	void operator=(TFace rvalue);
	bool operator==(TFace rvalue);
};


struct TAttributes
{
	GLfloat coord3d[3];
	GLfloat texcoord[2];
};

struct attributes_2f3f
{
	GLfloat coord2d[2];
	GLfloat v_color[3];
};

struct attributes_3f3f
{
	GLfloat coord3d[3];
	GLfloat v_color[3];
};

enum TReturns
{
	A_OK = 0,
	NO_OBJ = 1,
	NO_FILE = 1,
	WRONG_FILE = 3,
	ERROR = 0xFF
};

enum TFiles
{
	OBJ = 0,
	MTL = 1
};

#endif // DATASTRUCTURES
