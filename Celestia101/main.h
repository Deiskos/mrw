#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <SOIL/SOIL.h>

#include "gl/tobject/tobject.h"
#include "gl/mtl/material.h"
#include "gl/camera/camera.h"
#include "ui/player/player.h"
#include "utils/utils.h"
#include "utils/datastructures.h"
#include "gl/tobject/tobject.h"

GLuint program;

GLint attribute_coord3d;
GLint attribute_texcoord;
// GLint attribute_v_color;

GLint uniform_fade;
GLint uniform_u_color;
GLint uniform_mvp;
GLint uniform_tex;

// glm::vec3 nulvec3 = glm::vec3(0.0f, 0.0f, 0.0f);
// glm::vec4 nulvec4 = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
glm::vec3 unitv3  = glm::vec3(1.0f, 1.0f, 1.0f);

glm::mat4 projection;

bool pause = 1, dbg = 0;

FILE* logfile;

int Args(char *arg[]);


#endif