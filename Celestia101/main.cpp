/**
	Made by: Deiskos aka Denys Zheliezniak
	Linked libraries: -lglut -lGLEW -lGL -lm
	Comment: OMFG, that was I who wrote this?
**/
//TODO (<date>#<username>#<urgency>) <MSG>
//You may want to use "grep TODO -Rn *" to find all TODOs

//TODO (02.01.2016#deiskos#1) Add lighting to objects
//TODO (02.01.2016#deiskos#0) Add .mtl to load materials from Blender
//TODO (26.02.2016#deiskos#0) Check out OpenCL



#define GLM_FORCE_PURE
// #define GLM_SWIZZLE //<- DAFUQ?
// #include <sched.h>  //<- DAFUQ?
#include "main.h"


//--------------------------------------------------------------------
///Function definitions
void Simulate(void);



///Global objects

	Camera Cam1;
	Player Pl1;
	TUtility Utility;

//--------------------------------------------------------------------
///Class: TScreen

class TScreen
//TODO (31.12.2015#deiskos#9) move TScreen somewhere else
{
	public:
		TScreen();
		TScreenCoords Dimentions;	
		TScreenCoords Center;
		void CmpCenter(void);
} Screen;
TScreen::TScreen()
{
	Dimentions.u = 800;
	Dimentions.v = 600;

	CmpCenter();
}
void TScreen::CmpCenter(void)
{
	Center.u = (int)(Dimentions.u/2);
	Center.v = (int)(Dimentions.v/2);
}


//--------------------------------------------------------------------

TObject::TObject()
{

}

//TODO (28.12.2015#deiskos#8) Should implement rotating objects. That would be awesome.
// void TObject::objRotate3f(float degX, float degY, float degZ)
// {
// 	glm::vec3 axis_z(0.0f, 0.0f, 1.0f), axis_y(0.0f, 1.0f, 0.0f), axis_x(1.0f, 0.0f, 0.0f);
// 	rotation =
// 		glm::rotate(glm::mat4(1.0f), (float)(radPitch), axis_x)
// 		* glm::rotate(glm::mat4(1.0f), (float)(!mode ? radYaw : -radYaw), axis_y);
// }

int TObject::Initialize(const char *obj, const char *tex)
{
	Utility.OpenFile(obj);
	Utility.SetupObject(this);
	Utility.Reset();

	tex_id = SOIL_load_OGL_texture(
		tex,
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
	);

	glGenBuffers(1, &vbo_attrib);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_attrib);
	glBufferData(
		GL_ARRAY_BUFFER,
		vAttributes.size() * sizeof(TAttributes),
		vAttributes.data(),
		GL_STATIC_DRAW
	);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &ibo_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_elements);
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		vElements.size() * sizeof(GLushort),
		vElements.data(),
		GL_STATIC_DRAW
	);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	return 1;
}

void TObject::Visualize()
{
	model =
		glm::translate(
			glm::mat4(1.0f), //Identity matrix
			Position
		);

	mvp = projection * Cam1.GiveRotMatrix(0) * Cam1.getView() * model;

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(
		uniform_tex,
		0 //GL_TEXTURE0
	);
	glBindTexture(GL_TEXTURE_2D, tex_id);


	glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, glm::value_ptr(mvp));

	glBindBuffer(GL_ARRAY_BUFFER, vbo_attrib);
	glEnableVertexAttribArray(attribute_coord3d);
	glVertexAttribPointer(
		attribute_coord3d, // attribute
		3,                 // number of elements per vertex, here (x, y, z)
		GL_FLOAT,          // the type of each element
		GL_FALSE,          // take our values as-is
		sizeof(struct TAttributes),
		0//
	);

	glEnableVertexAttribArray(attribute_texcoord);
	glVertexAttribPointer(
		attribute_texcoord, // attribute
		2,                  // number of elements per vertex, here (x,y)
		GL_FLOAT,           // the type of each element
		GL_FALSE,           // take our values as-is
		sizeof(struct TAttributes),                  // no extra data between each position
		(GLvoid*) offsetof(struct TAttributes, texcoord)                   // offset of first element
	);


	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_elements);
	glDrawElements(GL_TRIANGLES, (vElements.size()), GL_UNSIGNED_SHORT, 0);

	glDisableVertexAttribArray(attribute_coord3d);
	glDisableVertexAttribArray(attribute_texcoord);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void TObject::setPosition(glm::vec3 NewPosition)
{
	Position = NewPosition;
}
glm::vec3 TObject::getPosition(void) const
{
	return Position;
}

//--------------------------------------------------------------------

TObject cube, pyramid, axis, sphere;


//--------------------------------------------------------------------
///Class: THookPoint

class THookPoint
{
	private:

		// TObject *g_ptr;
	public:
		float m;		
		glm::vec3 position, velocity, force_through;
		 THookPoint();
		~THookPoint();
		// void Visualize(void);
};

THookPoint::THookPoint()
{
	force_through = glm::vec3(0.0f, 0.0f, 0.0f);
	// g_ptr = NULL;
}

THookPoint::~THookPoint()
{

}

//--------------------------------------------------------------------
///Class: TBody



class TBody
{
	private:

	public:
		THookPoint h_p;

		int     (*f_ptr)(TBody&, float);//Указатель на функцию которая меняет координаты объекта
		TObject *obj;//Для вызова функции отрисовки текущего объекта

		glm::vec3 position;
		glm::vec3 velocity;
		glm::vec3 acceleration;
		glm::vec3 force;

		float m;

		TBody();
		~TBody();
		bool Init(void);
		bool ReHookObj (TObject*);
		bool ReHookFunc(int (*f_ptr)(TBody&, float));
		void Simulate (float);
		void Visualize(void);
};// fo_cube1, fo_cube2, fo_cube3;

TBody::TBody()
{
	f_ptr = NULL;
	// printf("%p\n", f_ptr);

	//position =		 glm::vec3(2.0f, 0.0f, 0.0f);
	//constVelocity = glm::vec3(1.0f, 0.0f, 0.0f);

	velocity =		 glm::vec3(0.0f, 0.0f, 0.0f);
	acceleration = glm::vec3(0.0f, 0.0f, 0.0f);
	force = glm::vec3(0.0f, 0.0f, 0.0f);

	m = 1.0f;

}
bool TBody::Init(void)
{
	h_p.m = m;
	h_p.position = position;
	h_p.velocity = velocity;
}
TBody::~TBody()
{

}

bool TBody::ReHookObj(TObject *input)
{
	if(input == NULL)
	{
		return 0;
	}
	else
	{
		obj = input;
		return 1;
	}
}
bool TBody::ReHookFunc(int (*ptr)(TBody &obj, float dt))
{
	if(ptr == NULL)
	{
		return 0;
	}	
	f_ptr = ptr;
	return 1;
}

void TBody::Simulate(float dt)
{
	force += h_p.force_through;
	f_ptr(*this, dt);

	//Setting values for new iteration;
	h_p.force_through = glm::vec3(0.0, 0.0, 0.0);
	h_p.position = position;
	h_p.velocity = velocity;
	force = glm::vec3(0.0, 0.0, 0.0);

}
void TBody::Visualize(void)
{
	obj->setPosition(position);
	obj->Visualize();
}



glm::vec3 g = glm::vec3(0.0f, -9.81f, 0.0f);

int funct(TBody &obj, float dt)
{
	// obj.force += obj.m * g;
	// obj.force -= 0.9f * obj.velocity; //Fading by environmnet
	obj.acceleration = (obj.force)/obj.m + g;
	obj.velocity += (obj.acceleration) * dt;
	// printf("%f, q%f, %f\n", obj.acceleration.x, obj.acceleration.y, obj.acceleration.z);
	// obj.velocity += g * dt;
	// obj.velocity -= glm::normalize(obj.velocity) * (float)(pow(glm::length(obj.velocity),2) * 0.1f) * dt;
	obj.position += obj.velocity * dt;
}
int dummy(TBody &obj, float dt)
{

}

//--------------------------------------------------------------------
///Class: Hook


class TForce
{
	private:
		std::vector<THookPoint*> vRegistry;

	public:
		char Name[64];
		float  (*f_ptr)(THookPoint&, THookPoint&);

		TForce();
		~TForce();

		void hookFunc(float  (*)(THookPoint&, THookPoint&));
		void newRegEntry(THookPoint*);
		int  delRegEntry(THookPoint*); 

		void Simulate(void);

		
};

TForce::TForce()
{

}
TForce::~TForce()
{

}
void TForce::hookFunc(float  (*ptr)(THookPoint&, THookPoint&))
{
	f_ptr = ptr;
}
void TForce::newRegEntry(THookPoint *entry)
{
	vRegistry.push_back(entry);
	std::sort(vRegistry.begin(), vRegistry.end());
}
int TForce::delRegEntry(THookPoint *entry)
{
	if(std::binary_search(vRegistry.begin(), vRegistry.end(), entry))
	{
		std::vector<THookPoint*>::iterator iPosition =
			std::find(vRegistry.begin(), vRegistry.end(), entry);
		vRegistry.erase(iPosition);
	}
}

void TForce::Simulate()
{
	for(int i = 0; i < vRegistry.size(); ++i)
	{
		for (int j = 0; j < vRegistry.size(); ++j)
		{
			if(i != j)
			{
				f_ptr(*vRegistry[i], *vRegistry[j]);		
			}
		}
	}

	fprintf(logfile, "Force(%p) simulated succesfully\n", this);
}


const float G = 6.67E-11;

float kforce(THookPoint& a, THookPoint& b)
{
	float k = 10;
	glm::vec3 nml;
	nml = nml = -glm::normalize(a.position - ((a.position + b.position)/2.0f));
	float dst;
	if((dst = glm::distance(a.position, b.position)) != 0)
	{
		float force_scalar = k * (dst - 5);

		a.force_through += ( nml) * (force_scalar);
		b.force_through += (-nml) * (force_scalar);
	}
}

float gforce(THookPoint& a, THookPoint& b) //Optimized
{
	// glm::vec3 pos1, pos2;
	// pos1 = zis.hptr1->position;
	// pos2 = zis.hptr2->position;
	float dst, force_scalar;

	glm::vec3 nml;
	// sumvec = (pos1 + pos2)/2.0f;
	nml = -glm::normalize(a.position - ((a.position + b.position)/2.0f));


	if((dst = glm::distance(a.position, b.position)) != 0)
	{
		force_scalar = G * a.m * b.m/(pow(dst, 2));

		a.force_through += ( nml) * (force_scalar);
		b.force_through += (-nml) * (force_scalar);
	}
	else
	{
		// s = glm::vec3(0.0, 0.0, 0.0);
	}

	// return force_scalar;
}



//--------------------------------------------------------------------
///Class: Container

class TContainer
{
	private:
		std::vector<TBody*> vBody_Register;
		bool newTBody;
		std::vector<TForce*> vForce_Register;
		bool newTForce;

	public:
		TContainer();
		~TContainer();

		void newBody(
			TObject*,
			int (*)(TBody&, float),
			glm::vec3,
			glm::vec3
		);
		void delBody(float, float, float);

		void newForce(float  (*)(THookPoint&, THookPoint&));
		void delForce(int);

		void Simulate(float, float);
		void Visualize(void);
} Container;

TContainer::TContainer(void)
{
	newTBody = false;
}
TContainer::~TContainer()
{

}

void TContainer::newBody(
	TObject* obj,
	int (*f_ptr)(TBody&, float),
	glm::vec3 position,
	glm::vec3 velocity
)
{
	newTBody = true;
	vBody_Register.push_back(new TBody);
	vBody_Register.back()->ReHookObj(obj);
	vBody_Register.back()->ReHookFunc(f_ptr);
	vBody_Register.back()->position += position;
	vBody_Register.back()->velocity += velocity;
	vBody_Register.back()->Init();
}
void TContainer::delBody(float r, float g, float b)
{
	//!!!
}

void TContainer::newForce(float  (*ptr)(THookPoint&, THookPoint&))
{

	vForce_Register.push_back(new TForce);
	vForce_Register.back()->hookFunc(ptr);
	for(int i = 0; i < vBody_Register.size(); ++i)
	{
		vForce_Register.back()->newRegEntry(&(*vBody_Register[i]).h_p);
		fprintf(logfile, "poke\n");
	}

}

void TContainer::Simulate(float dT, float dt)
{
	int n = (int)(dT/dt);
	for(int i = 0; i <= n; i++)
	{
		for(int j = 0; j < vForce_Register.size(); ++j)
		{
			vForce_Register[j]->Simulate();
		}


		for(int j = 0; j < vBody_Register.size(); ++j)
		{
			vBody_Register[j]->Simulate(dt);
		}
	}
}

void TContainer::Visualize(void)
{
	for(int j = 0; j < vBody_Register.size(); ++j)
	{
		vBody_Register[j]->Visualize();
	}
}

//--------------------------------------------------------------------
///Class: TGContainer

class TGContainer
{
private:
	std::vector<TObject*> vGraph_Registry;
	std::vector<TMaterial*> vMaterial_Registry;

public:


};

//--------------------------------------------------------------------
///GLUT functions
bool rmb_actmotion = 0;

void mouse(int button, int state, int x,int y)
{
	// states
	// {
	// 	 GLUT_DOWN,
	// 	 GLUT_UP
	//  }
	// buttons
	// {
	//   GLUT_LEFT_BUTTON
	//   GLUT_RIGHT_BUTTON
	// }
	switch(state)
	{
		case GLUT_DOWN:
			switch(button)
			{
				case GLUT_RIGHT_BUTTON:
					// rmb_actmotion = true;
					glutWarpPointer(Screen.Center.u, Screen.Center.v);		
				break;
			}
		break;
		case GLUT_UP:
			switch(button)
			{
				case GLUT_RIGHT_BUTTON:
					rmb_actmotion = !rmb_actmotion;
					//glutWarpPointer(Screen.Dimentions.u, Screen.Dimentions.v);
				break;
			}
		break;
		default :
			exit(1);
		break;
	}
}

const float deltaangle = 0.1f;
void mouseactivemotion(int x,int y)
{

}

void mousepassivemotion(int x, int y)
{
	if(rmb_actmotion)
	{
		// glutWarpPointer(Screen.Dimentions.u, Screen.Dimentions.v);
		glm::vec2 CurMousePos = glm::vec2(x, y);
		glm::vec2 Center = glm::vec2(Screen.Center.u, Screen.Center.v);
		glm::vec2 Direction = CurMousePos - Center;
		float dst = glm::distance(CurMousePos, Center);//, len = glm::length(Direction);
		if(rmb_actmotion && (dst > 0.001f))
		{

			Pl1.DeltaPitchYaw(-Direction.y * deltaangle, Direction.x * deltaangle);
			glutWarpPointer(Screen.Center.u, Screen.Center.v);
		}
	}

}


void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(program);

	Simulate();

	Container.Visualize();

	// if(dbg)
	// {	
	// 	axis.setPosition(Pl1.getPosition() + unitv3 * Cam1.getLookAt());
	// 	axis.Visualize();
	// }

	glutSwapBuffers();
}

void Timer(int value)
{
	glutPostRedisplay();
	glutTimerFunc(
		2,
		Timer,
		1
	);
}

void onReshape(int width, int height)
{
	Screen.Dimentions.u = width;
	Screen.Dimentions.v = height;
	Screen.CmpCenter();

	projection =
		glm::perspective(
			45.0f,                              //FOV
			1.0f * Screen.Dimentions.u/Screen.Dimentions.v,    //Aspect ratio
			0.1f,                               //Near
			1000.0f                               //Far
		);

	glViewport(0, 0, Screen.Dimentions.u, Screen.Dimentions.v);
}


void Keyboard(unsigned char key, int x, int y)
{
	Pl1.KBD(key);
	switch (key)
	{
		case 'q':
				exit(0);
		break;
		
		case '.':
			//mpl *= 2;
		break;
		case ',':
			//mpl /= 2;
		break;
		case 'p':
			pause = !pause;
		break;
		case '`':
			dbg = !dbg;
		break;
		default:

		break;
	}
}


//--------------------------------------------------------------------
///Else

int init_resources(void)
//TODO (28.12.2015#deiskos#9) refactor init_resources()
{
		GLint link_ok = GL_FALSE;

		GLuint vs, fs;
		if ((vs = create_shader("shaders/shader.v", GL_VERTEX_SHADER))   == 0)
			return 0;
		if ((fs = create_shader("shaders/shader.f", GL_FRAGMENT_SHADER)) == 0)
			return 0;


		program = glCreateProgram();
		glAttachShader(program, vs);
		glAttachShader(program, fs);
		glLinkProgram(program);
		glGetProgramiv(program, GL_LINK_STATUS, &link_ok);
		if(link_ok == 0)
		{
			fprintf(stderr, "Error while linking GLSL program");
			print_log(program);
			return 0;
		}

	///Attrib section
		const char* attribute_name = "coord3d";
		attribute_coord3d = glGetAttribLocation(program, attribute_name);
		if (attribute_coord3d == -1)
		{
			fprintf(stderr, "Could not bind attribute\"%s\"\n", attribute_name);
			return 0;
		}

		attribute_name = "texcoord";
		attribute_texcoord = glGetAttribLocation(program, attribute_name);
		if (attribute_texcoord == -1) {
			fprintf(stderr, "Could not bind attribute \"%s\"\n", attribute_name);
			return 0;
		}
	//EndOf Attrib seciton

	///Uniform section
		const char* uniform_name;

		// uniform_name = "fade";
		// uniform_fade = glGetUniformLocation(program, uniform_name);
		// if (uniform_fade == -1)
		// {
		// 	fprintf(stderr, "Could not bind uniform \"%s\"\n", uniform_name);
		// 	print_log(uniform_fade);
		// 	return 0;
		// }

		uniform_name = "mvp";
		uniform_mvp = glGetUniformLocation(program, uniform_name);
		if (uniform_mvp == -1) 
		{
			fprintf(stderr, "Could not bind uniform \"%s\"\n", uniform_name);
			print_log(uniform_mvp);
			return 0;
		}

	//EndOf Uniform seciton

	//Graphic object initialize sequence
		if(!cube.Initialize("data/objects/cube.obj", "data/textures/dice.png"))
		{
			return 0;
		}
	//EndOf Graphic object initialize sequence
	//TContainer init
		Container.newBody(
			&cube,
			dummy,
			glm::vec3(0.0, 0.0, 0.0),
			glm::vec3(5.0, 0.0, 0.0)
		);	
		Container.newBody(
			&cube,
			funct,
			glm::vec3(0.0, -5.0, 0.0),
			glm::vec3(0.0, 0.0, 0.0)
		);
		// Container.newBody(
		// 	funct,
		// 	glm::vec3(5.0, 0.0, 0.0),
		// 	glm::vec3(5.0, 0.0, 0.0)
		// );
		Container.newForce(kforce);



	//EndOf TContainer init

		Pl1.HookCamPtr(&Cam1);

	return 1;
}

void Simulate(void)
{
	Cam1.PlaceCamera(Pl1.Process());



	if(!pause)
	{
		Container.Simulate(0.016f, 0.0001f);
	}
}

void free_resources()
{
	glDeleteProgram(program);
}

int Args(char *arg[])
{

}



int main(int argc, char *argv[])
{
	logfile = fopen("log/log", "w");
	// printf("\n%s", argv[1]);
	//exit(0);
	if(argc>0)
	{
		//Args(argv);
	}
	
	glutInit(&argc, argv);
	glutInitContextVersion(2,0);

	glutInitDisplayMode(GLUT_RGBA|GLUT_ALPHA|GLUT_DOUBLE|GLUT_DEPTH);
	glutInitWindowSize(Screen.Dimentions.u, Screen.Dimentions.v);
	glutCreateWindow("Modern OpenGL");

	GLenum glew_status = glewInit();
	if(glew_status != GLEW_OK)
	{
		fprintf(stderr, "Error: %s\n",glewGetErrorString(glew_status));
		return EXIT_FAILURE;
	}
	if (!GLEW_VERSION_2_0) {
		fprintf(stderr, "Error: your graphic card does not support OpenGL 2.0\n");
		return 1;
	}

	// Enable alpha
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.0, 0.3, 0.0, 1.0);

	if(init_resources())
	{
		glutMouseFunc(mouse);
		// void glutMotionFunc(void (*func) (int x,int y));
		// glutMotionFunc(mouseactivemotion);
		// void glutPassiveMotionFunc(void (*func) (int x, int y));
		glutPassiveMotionFunc(mousepassivemotion);
		glutDisplayFunc(display);
		glutReshapeFunc(onReshape);
		glutKeyboardFunc(Keyboard);
		glutTimerFunc(15, Timer, 1);
		glutMainLoop();
	}

	free_resources();
	return EXIT_SUCCESS;
}
